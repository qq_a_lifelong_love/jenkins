# 使用官方的 OpenJDK 8 镜像作为基础镜像
FROM openjdk:8-jdk-alpine

# 将本地文件添加到容器中
COPY target/jenkins_test-0.0.1-SNAPSHOT.jar  /app/app.jar
RUN ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
RUN echo 'Asia/Shanghai' >/etc/timezone

# 指定容器启动时要运行的命令
CMD ["java", "-jar", "/app/app.jar"]

# docker run --env JVM_OPTS="-Xms512m -Xmx1024m" -p 8080:8080 my-app